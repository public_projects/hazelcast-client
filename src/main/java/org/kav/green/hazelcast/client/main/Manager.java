package org.kav.green.hazelcast.client.main;

import com.hazelcast.cache.HazelcastExpiryPolicy;
import com.hazelcast.cache.ICache;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;

@Service
public class Manager implements CommandLineRunner {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Test Map
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getNetworkConfig().addAddress("127.0.0.1");

        HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);
        try {
            System.out.println(clientConfig);

            BlockingQueue<String> queue = client.getQueue("queue");
            queue.put("Hello!");
            System.out.println("Message sent by Hazelcast Client!");

            IMap<String, String> newMap = client.getMap("user-account");
            newMap.put("1", "user1");
            newMap.put("2", "user2");
            newMap.put("3", "user3");

            getMaps(client);

            ICache iCache = client.getCacheManager().getCache("user-cache");
            iCache.put(new String("c1"), new String("dataC1"), new HazelcastExpiryPolicy());
        } finally {
            client.shutdown();
        }
    }

    /**
     * Get all the maps in hazelcast instance.
     *
     * @param client
     */
    public void getMaps(HazelcastInstance client) {
        Collection<DistributedObject> instances = client.getDistributedObjects();
        int count = 1;
        for (DistributedObject instance : instances) {
            if (instance.getServiceName().equals("hz:impl:mapService")) {
                log.info("{}. map name: [{}][size: {}]", count++, instance.getName(), client.getMap(instance.getName()).size());
            }
        }
    }
}
